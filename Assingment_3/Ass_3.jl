### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 410c4a50-b42b-11eb-28b6-0bdc1cc9f6af
begin 
	using Pkg
	Pkg.activate("project.toml")
	using PlutoUI
	using Markdown
	using InteractiveUtils
end

# ╔═╡ 58c3225d-6bc8-480f-a980-39f877a087bf
begin 
	@enum C_D One Two Three Four
	C_D
	mutable struct CCSPvariable
		val::Union{Nothing,C_D}
		forbidden_vars::Vector{C_D}
		name::String
		domain_restriction_count::Int64
	end
	rand(setdiff(Set([1,2,3, 4]),Set([2,3,4])))
	struct C_CSP
	vari::Vector{CCSPvariable}
	constraints::Vector{Tuple{CCSPvariable,CCSPvariable}}
end
end 

# ╔═╡ fab41c59-22ae-483d-94f4-88df906fc2ce
rand(setdiff(Set([1,2,3, 4]),Set([2,3,4])))

# ╔═╡ a7f25002-3354-47bb-940b-159cfd725f07
begin 
	one = CCSPvariable(nothing, [],"one", 0)
	two = CCSPvariable(nothing, [],"two", 0)
	three = CCSPvariable(nothing, [Two, Three, Four],"three", 0)
	four = CCSPvariable(nothing, [],"four", 0)
	
	#End Here
end

# ╔═╡ 019552ce-4995-4cdc-8189-3f1aee7d72a1
function solver(e::C_CSP, all_assignments)
		for var in e.vari
			if var.domain_restriction_count == 4
				return []
			else
next_value = rand(setdiff(Set([one,two,three,four]), Set(var.forbidden_vars)))

					#variable assignment
			    println(var.forbidden_vars)
				  var.val = next_val
			  
			
		end	
			#end here
return all_assignments
end
	end

# ╔═╡ 032b4ab4-fd4c-4b69-9103-cfe498228f24
#Problem Definition

# ╔═╡ 404301dd-e330-4529-a038-00b730f2cdd5
#Function call for solver

# ╔═╡ 4a909487-b236-4589-b5f2-25ae3cdd2b1b


# ╔═╡ 1b0fcf85-0f1a-40a3-8dcd-07e986947325


# ╔═╡ Cell order:
# ╠═410c4a50-b42b-11eb-28b6-0bdc1cc9f6af
# ╠═58c3225d-6bc8-480f-a980-39f877a087bf
# ╠═fab41c59-22ae-483d-94f4-88df906fc2ce
# ╠═019552ce-4995-4cdc-8189-3f1aee7d72a1
# ╠═a7f25002-3354-47bb-940b-159cfd725f07
# ╠═032b4ab4-fd4c-4b69-9103-cfe498228f24
# ╠═404301dd-e330-4529-a038-00b730f2cdd5
# ╠═4a909487-b236-4589-b5f2-25ae3cdd2b1b
# ╠═1b0fcf85-0f1a-40a3-8dcd-07e986947325
