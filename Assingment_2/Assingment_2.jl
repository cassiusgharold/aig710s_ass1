### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ b9732cd5-ce69-40df-bbf7-ad852f859b84
using Pkg

# ╔═╡ 24f145b0-b42a-11eb-1fff-c5f5eab92d5e
using Markdown

# ╔═╡ f02d2d1d-bf83-4742-94fc-caf3190f1d19
using InteractiveUtils

# ╔═╡ a585bce1-6a72-490e-9867-13a2f3c17a01
function minimax(depth, nodeIndex, maximizingPlayer,values, alpha, beta)
	if depth == 3
		return values[nodeIndex]
	end
		if maximizingPlayer
	
		best = MIN
		
			for i in range(0, 2)
			
			val = minimax(depth - 1,
						nodeIndex,False, values, alpha, beta)
			best = max(best, val)
			alpha = max(alpha, best)

			# Alpha Beta Pruning
			if beta <= alpha
				break
			end
		end
		return best
			else
		best = MAX

		# Recur for left and
		# right children
		for i in range(0, 2)
		
			val = minimax(depth - 1, nodeIndex,True, values, alpha, beta)
			best = min(best, val)
			beta = min(beta, best)

			# Alpha Beta Pruning
			if beta <= alpha
				break
			end
		end
	end


end

# ╔═╡ 1685a9ab-223d-4e5a-89da-ca376a38b9d5
values = [78,5,10,1,4,3,1,311,2,8,19,2,7,3,3,8,9,0,3,4,6,7,5,3,2,6]


# ╔═╡ ddfa745d-51ab-4ff7-8f6d-7124df7836e0
minimax(3,1,true,values,max,min)


# ╔═╡ Cell order:
# ╠═24f145b0-b42a-11eb-1fff-c5f5eab92d5e
# ╠═f02d2d1d-bf83-4742-94fc-caf3190f1d19
# ╠═b9732cd5-ce69-40df-bbf7-ad852f859b84
# ╠═a585bce1-6a72-490e-9867-13a2f3c17a01
# ╠═1685a9ab-223d-4e5a-89da-ca376a38b9d5
# ╠═ddfa745d-51ab-4ff7-8f6d-7124df7836e0
